# Week 7 Mini-Project
## Author
Ziyu Shi

## Requirements
### Data Processing with Vector Database
- Ingest data into Vector database
- Perform queries and aggregations
- Visualize output

## Project Instructions
1. Create a new Rust project with `cargo new <PROJECT_NAME>`
2. Add necessary dependencies to the Cargo.toml
    ```
    cargo add qdrant-client anyhow tonic tokio serde-json --features tokio/rt-multi-thread
    ```
3. Launch the Qdrant database with enabled gRPC interface
    ```
    docker run -p 6333:6333 -p 6334:6334 \
        -e QDRANT__SERVICE__GRPC_PORT="6334" \
        qdrant/qdrant
    ```
4. Implement required operations in `main.rs` and use `cargo run` to test output.

## Sreenshots
### Connect to Qdrant vector database
![image](images/connectDB.png)

### Ingest data into database
Loop five times to create and ingest five different data points into the collection. Each point has a vector `(vec![12.0 + i as f32; 10])`, where `i` varies in each iteration to slightly alter the vector values, and a payload that contains structured data `(foo, bar, baz.qux)`, which also varies in each iteration.
![image](images/ingest.png)

### Perform queries
Perform a query against the collection looking for points that are similar to a query vector `(vec![11.; 10])`. The query does not apply any filters `(filter: None)`, requests up to 5 points `(limit: 5)`, and includes payload data in the results `(with_payload: Some(true.into()))`.
![image](images/query.png)

### Visualize the output
Iterate over the results of the query, printing out the payload of each found point.
![image](images/visualization.png)

